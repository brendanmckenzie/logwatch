﻿open System
open System.IO
open System.Text

let watchPath path =
  let mutable files = Directory.GetFiles(path) |> Array.toList |> List.map (fun fn -> ( fn, FileInfo(fn).Length ))

  printfn "watching %s: %d file(s)" path files.Length

  let fsw = new FileSystemWatcher()
  fsw.Path <- path
  fsw.EnableRaisingEvents <- true
  fsw.IncludeSubdirectories <- true
  fsw.SynchronizingObject <- null

  fsw.Changed.Add(fun ev ->
    printfn "changed %s" ev.FullPath
    let (_, oldSize) = List.find (fun (fn, _) -> fn = ev.FullPath) files
    let newSize = FileInfo(ev.FullPath).Length
    let diff = (int)oldSize - (int)newSize
    
    let buffer = Array.init diff (fun i -> byte(0))
    let fs = new FileStream(ev.FullPath, FileMode.Open)
    fs.Seek(oldSize, SeekOrigin.Begin) |> ignore
    fs.Read(buffer, 0, diff) |> ignore
    fs.Dispose()
    files <- List.filter (fun (fn, _) -> fn <> ev.FullPath) files
    files = ( ev.FullPath, newSize ) :: files |> ignore

    printfn "%s" (Encoding.UTF8.GetString buffer))

  fsw.Created.Add(fun ev ->
    printfn "created %s" ev.FullPath
    files = ( ev.FullPath, (int64)0 ) :: files |> ignore)
  
  fsw.Deleted.Add(fun ev ->
    printfn "deleted %s" ev.FullPath
    files = List.filter (fun (fn, _) -> fn <> ev.FullPath) files |> ignore)
  
  fsw.Renamed.Add(fun _ -> printfn "renamed")
  
[<EntryPoint>]
let main argv =
  printfn "Hello World from F#!"
  match argv with
  | [|path|] ->
    watchPath path
  | _ -> printfn "Specify a path to watch"

  printfn "Press any key to continue . . ."
  Console.ReadKey(true) |> ignore

  0